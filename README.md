# Contacts app

## Description

This app calls an api which retrieves random users. These users are grouped by the first letter of their last name. Users can then be viewed by clicking on the correct letter tabs corresponding to their last name.

## To run project

1. run npm install in the ContactsApp directory
2. run npm run dev to start the project

## To run tests

1. run npm run test in the ContactsApp directory

### Live version

https://contacts-app-martin.netlify.app/
