import { configJson } from "./config";
import Contacts from "./features/Contacts/Contacts";
import "./App.styles.scss";

const App = () => {
	const title = configJson.title;
	return (
		<div className="app">
			<h1
				style={{ textAlign: "center", fontSize: "24px", marginBottom: "16px" }}
			>
				{title}
			</h1>
			<Contacts />
		</div>
	);
};

export default App;
