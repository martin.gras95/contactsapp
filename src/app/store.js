import { configureStore } from "@reduxjs/toolkit";
import contactsSlice from "../features/Contacts/contactsSlice";

const store = configureStore({
	reducer: {
		contacts: contactsSlice.reducer,
	},
});

export default store;
