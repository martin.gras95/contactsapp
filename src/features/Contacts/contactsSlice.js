import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { contactGroups, statusCodes } from "./constants";
import axios from "axios";
import { configJson } from "../../config";
import { groupUsersByFirstLetter } from "./helpers";

const apiUrl = configJson.userUrl;

export const getContacts = createAsyncThunk("get-contacts", async () => {
	const response = await axios.get(apiUrl);

	// Group by first letter of last name before returning
	const groupedResponse = groupUsersByFirstLetter(response.data.results);

	return { ...contactGroups, ...groupedResponse };
});

const initialState = {
	data: {
		...contactGroups,
	},
	status: statusCodes.IDLE,
	currentTab: "A",
	selectedContact: {},
};

const contactsSlice = createSlice({
	name: "contacts",
	initialState: initialState,
	reducers: {
		updateCurrentTab: (state, action) => {
			state.currentTab = action.payload;
		},
		updateSelectedContact: (state, action) => {
			state.selectedContact = action.payload;
		},
	},
	extraReducers: (builder) => {
		builder
			.addCase(getContacts.fulfilled, (state, action) => {
				state.data = action.payload;
				state.status = statusCodes.SUCCESS;
			})
			.addCase(getContacts.pending, (state) => {
				state.status = statusCodes.LOADING;
			})
			.addCase(getContacts.rejected, (state) => {
				state.status = statusCodes.ERROR;
			});
	},
});

export default contactsSlice;
