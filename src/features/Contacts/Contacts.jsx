import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Contact, ContactCard, TabBar } from "./components";
import { statusCodes } from "./constants";
import { getContacts } from "./contactsSlice";
import "./Contacts.styles.scss";

// Description
// Main Contacts feature container

const Contacts = () => {
	const contacts = useSelector((state) => state.contacts);
	const { status, data, currentTab } = contacts;
	const dispatch = useDispatch();

	// Grab contacts
	useEffect(() => {
		dispatch(getContacts());
	}, [dispatch]);

	return status === statusCodes.LOADING ? (
		<div>Loading</div>
	) : status === statusCodes.ERROR ? (
		<div>Failed to load data</div>
	) : (
		<>
			<div className="contacts-container">
				<TabBar />
				<div className="contacts-entries">
					{data[currentTab].map((contact) => (
						<Contact key={JSON.stringify(contact)} contact={contact} />
					))}
				</div>
				<ContactCard />
			</div>
		</>
	);
};

export default Contacts;
