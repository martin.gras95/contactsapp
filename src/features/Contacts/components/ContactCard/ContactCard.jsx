import React from "react";
import { useDispatch, useSelector } from "react-redux";
import contactsSlice from "../../contactsSlice";
import "./ContactCard.styles.scss";

// Description
// Card component displaying user details.

const ContactCard = () => {
	const contacts = useSelector((state) => state.contacts);
	const { selectedContact } = contacts;
	const dispatch = useDispatch();
	const { updateSelectedContact } = contactsSlice.actions;

	// Grab display data from the the state
	const cardDetailsData = {
		email: selectedContact?.email || "",
		phone: selectedContact?.phone || "",
		street: selectedContact?.location?.street?.name || "",
		city: selectedContact?.location?.city || "",
		sate: selectedContact?.location?.state || "",
		postcode: selectedContact?.location?.postcode || "",
	};

	return selectedContact.id ? (
		<div className="contact-card">
			<span className="contact-name">
				{selectedContact.name.last}, {selectedContact.name.first}
			</span>
			<div className="contact-details">
				<img src={selectedContact.picture.large} alt="contact picture" />
				<table>
					<tbody>
						{Object.keys(cardDetailsData).map((key) => (
							<tr key={JSON.stringify(cardDetailsData[key])}>
								<td className="data-key">{key} </td>
								<td className="data-value">{cardDetailsData[key]}</td>
							</tr>
						))}
					</tbody>
				</table>
			</div>
			<button onClick={() => dispatch(updateSelectedContact({}))}>Close</button>
		</div>
	) : null;
};

export default ContactCard;
