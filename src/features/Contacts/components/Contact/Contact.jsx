import React from "react";
import { useDispatch, useSelector } from "react-redux";
import contactsSlice from "../../contactsSlice";
import "./Contact.styles.scss";
import PropTypes from "prop-types";

// Description
// Individual contact buttons in the list.

const Contact = ({ contact }) => {
	const dispatch = useDispatch();
	const contacts = useSelector((state) => state.contacts);
	const { selectedContact } = contacts;

	const { updateSelectedContact } = contactsSlice.actions;

	// Check if this user is selected
	const isActive = selectedContact?.name?.first === contact.name.first;

	return (
		<button
			className={`contact-button contact-button-${
				isActive ? "active" : "idle"
			}`}
			onClick={() => dispatch(updateSelectedContact(contact))}
		>
			<span>
				{contact.name.first}, {contact.name.last}
			</span>
		</button>
	);
};

Contact.propTypes = { contact: PropTypes.object };

export default Contact;
