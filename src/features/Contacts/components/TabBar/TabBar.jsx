import React from "react";
import { Tab } from "../";
import { tabs } from "./constants";
import "./TabBar.styles.scss";

// Description
// Container for letter tabs

const TabBar = () => {
	return (
		<div className="tabbar">
			{tabs.map((tab) => (
				<Tab key={tab} tab={tab} />
			))}
		</div>
	);
};

export default TabBar;
