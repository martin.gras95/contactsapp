import React from "react";
import { useDispatch, useSelector } from "react-redux";
import contactsSlice from "../../contactsSlice";
import "./Tab.styles.scss";
import PropTypes from "prop-types";

// Description
// Clickable letter tabs

const Tab = ({ tab }) => {
	const dispatch = useDispatch();
	const contacts = useSelector((state) => state.contacts);
	const { data, currentTab } = contacts;
	const { updateCurrentTab } = contactsSlice.actions;

	// Number of contacts per tab
	const numberOfContacts = data[tab.toUpperCase()].length;

	return (
		<button
			disabled={!numberOfContacts}
			onClick={() => dispatch(updateCurrentTab(tab.toUpperCase()))}
			className={`tab-button ${
				currentTab === tab.toUpperCase() ? "active" : ""
			}`}
		>
			<span className="tab-letter">{tab}</span>
			<span className="tab-amount">{numberOfContacts}</span>
		</button>
	);
};

Tab.propTypes = {
	tab: PropTypes.string,
};

export default Tab;
