// Group users by first letter of their first name
export function groupUsersByFirstLetter(users) {
	let result = {};

	for (let i = 0; i < users.length; i++) {
		let firstLetter = users[i].name.last[0].toUpperCase();
		// Create array for each unique letter
		if (!result[firstLetter]) {
			result[firstLetter] = [];
		}
		// Push to array
		result[firstLetter].push(users[i]);
	}

	return result;
}
