import { groupUsersByFirstLetter } from "./helpers";

describe("groupUsersByFirstLetter", () => {
	test("returns contacts grouped by the first letter of their last name", () => {
		const users = [
			{ name: { first: "John", last: "Smith" } },
			{ name: { first: "Sam", last: "Jones" } },
			{ name: { first: "Sally", last: "Johnson" } },
			{ name: { first: "Roy", last: "Doe" } },
		];
		const expected = {
			S: [{ name: { first: "John", last: "Smith" } }],
			J: [
				{ name: { first: "Sam", last: "Jones" } },
				{ name: { first: "Sally", last: "Johnson" } },
			],
			D: [{ name: { first: "Roy", last: "Doe" } }],
		};
		const result = groupUsersByFirstLetter(users);
		expect(result).toEqual(expected);
	});
});
